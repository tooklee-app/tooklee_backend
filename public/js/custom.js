$(document).ready(function () {
    $('.addVehicles').click(function () {
        var manufacturer = $('#manufacturer').val();
        var data = { manufacturer: manufacturer };
        saveData(data, 1);
    })

    $(".addManufacturer").click(function () {
        $("#vehicleadd").modal();
    });

    $(".addModel").click(function () {
        var model_name = $('#model_name').val();
        var manufacturer_id = $('#manufacturer_detail').val();
        if (model_name.length == 0 || manufacturer_id.length == 0) {
            alert('Please select model name and manufacturer detail');
            return false;
        }
        var data = { model_name: model_name, manufacturer_id: manufacturer_id };
        saveData(data, 2);
    });
    $('.closeBtn').click(function () {
        window.location.reload();
    });

});

function saveData(data, reqType) {
    $.ajax({
        type: 'POST',
        url: '/save-vehicle-info',
        dataType: 'JSON',
        data: { data, reqType },
        success: function (resdata) {
            if (reqType == 1) {
                $('#manufacturer').val('');
            }
            alert(resdata.msg);
        }
    });
}