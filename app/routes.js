/**
 * Api Controllers
 */
const auth = require('./controllers/auth');
const profile = require('./controllers/profile');
const offerRide = require('./controllers/offer_ride');
/**
 * Web Controllers
 */
const manageVehicles = require('./controllers/web/manage_vehicles');
/**
 * Middleware
 */
const validateBasicAuth = require('./middleware/validateBasicAuth');

module.exports = function (app) {
  /*
    *   Account Setup Api Start
    */
  app.post('/auth', auth.index);
  app.post('/auth/register', auth.register);
  app.post('/auth/verify-otp', validateBasicAuth, auth.verifyOtp);
  app.post('/auth/send-email-otp', validateBasicAuth, auth.send_otp_email);
  app.post('/auth/send-phone-otp', validateBasicAuth, auth.send_otp_phone);
  app.put('/auth/update-phone', validateBasicAuth, auth.updatePhone);
  app.put('/auth/create-session', validateBasicAuth, auth.createSession);
  /*
    *   Offser a Ride Api Start
    */
  app.get('/offer-ride/vehicles', validateBasicAuth, offerRide.getVehicles);
  app.post('/offer-ride/vehicle', validateBasicAuth, offerRide.manageVehicle);
  app.get('/offer-ride/vehicle', validateBasicAuth, offerRide.getVehicle);
  app.post('/offer-ride/drivingLicense', validateBasicAuth, offerRide.manageDrivingLicence);
  app.get('/offer-ride/drivingLicense', validateBasicAuth, offerRide.getDrivingLicense);
  app.post('/offer-ride/payment', validateBasicAuth, offerRide.managePayment);
  /*
    *   Profile Setup Api Start
    */
  app.put('/profile/update-profile', validateBasicAuth, profile.updateProfile);
  app.post('/create-profile', validateBasicAuth, profile.index);
  /*
    *   Profile Setup Api End
    */
  app.delete('/auth/delete', auth.deleteAccount);
  /**
     * Web routes Start
     */
  app.get('/add-vehicles', manageVehicles.add_vehicles);
  app.post('/save-vehicle-info', manageVehicles.save_Data);
};
