/**
 * Config files
 */
const jwt = require('jsonwebtoken');
const userModel = require('../models/user');
const httpCode = require('../constants/http_codes');
const lang = require('../constants/en_messages');

module.exports = async function (req, res, next) {
  const respArr = {};
  const token = req.headers['x-access-token'];
  try {
    const decoded = await jwt.verify(token, process.env.SECRET);
    req.user = await userModel.findOne({ _id: decoded.id }).exec();
    next();
  } catch (error) {
    respArr.code = httpCode.basic_auth_failed;
    respArr.msg = lang.auth_failed;
    respArr.data = {};
    res.json(respArr);
  }
};
