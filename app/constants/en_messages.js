module.exports = {
  success: 'Success.',
  error: 'Some Error Occurred.',
  invalid_login: 'This number is not registered with us.',
  auth_failed: 'Authentication Failed Please try again.',
  token_missing: 'Token is required.',
  param_missing: 'Required parameter missing.',
  invalid_phone: 'Please enter a valid phone number.',
  invalid_email: 'Please enter a valid email.',
  invalid_token: 'Failed to authenticate token.',
  invalid_image: 'Please upload a valid image.',
  req_sent: 'Request Sent Successfully.',
  try_again: 'Please try again.',
  invalid_req: 'Invalid req type.',
  request_already_received: 'You have already received request from sender.',
  request_already_sent: 'You have already sent request to receiver.',
  already_friends: 'You are already friends.',
  no_requests: 'No requests found.',
  no_data: 'No data found.',
  otp_sent_success: 'OTP has been sent to your mobile no.',
  email_otp_sent_success: 'OTP has been sent to your email Id.',
  otp_verified: 'OTP verified successfully.',
  invalid_otp: 'Invalid OTP please try again.',
  email_exists: 'This email is already registered with us.',
  email_not_exists: 'This email is not registered with us.',
  token_expired: 'Your session is expired login please again.',
  phone_not_exists: 'This phone no. is not registered with us.',
  phone_exists: 'This number is already registered with us.',
  same_phone: 'This number is already your registered mobile number.',
};
