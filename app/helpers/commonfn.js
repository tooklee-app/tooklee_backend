const config = require('config');
const fs = require('fs');
const dateTime = require('node-datetime');
const ejs = require('ejs');
const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SEND_GRID_API_KEY);
const SendOtp = require('sendotp');
const constants = require('../constants/http_codes');

const sendOtp = new SendOtp(process.env.OTP_API_KEY);
let respArr = {};

exports.getDateTim = () => {
  const dt = dateTime.create();
  const formatted = dt.format('Y-m-d H:M:S');
  return formatted;
};

exports.getErrMsg = (errorArr) => {
  const msg = [];
  for (const i in errorArr) {
    msg[i] = errorArr[i].msg;
  }
  return msg;
};

exports.sendOtpCode = (phone, otp) => {
  sendOtp.send(phone, config.get('sender_id'), otp, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log('Otp sent');
    }
  });
};

exports.validateImage = (file, validTypes) => new Promise((resolve) => {
  const oldpath = file.path;
  let fileExt = file.originalFilename.replace(/^.*\./, '');
  fileExt = fileExt.toLowerCase();
  if (validTypes.indexOf(fileExt) === -1) {
    respArr.code = constants.invalid_image;
    respArr.msg = global.lang.invalid_image;
    respArr.data = {};
    resolve(respArr);
  } else {
    const newFileName = `${Date.now()}.${fileExt}`;
    const newpath = `${global.filePath}/public/uploads/${newFileName}`;
    fs.rename(oldpath, newpath, (err) => {
      if (err) {
        respArr.code = constants.error;
        respArr.msg = global.lang.error;
        respArr.data = {};
        resolve(respArr);
      } else {
        respArr.code = constants.success;
        respArr.msg = global.lang.success;
        respArr.data = newFileName;
        resolve(respArr);
      }
    });
  }
});
/*
 * Mandatory params validation
 */
exports.validateParams = (req, reqParams, callback) => {
  let isValidated = true;
  for (let i = 0; i < reqParams.length; i++) {
    const field = reqParams[i];
    const fieldValue = (req[field]) ? req[field] : '';
    if (fieldValue.length === 0) {
      isValidated = false;
      respArr.code = constants.param_missing;
      respArr.msg = global.lang.param_missing;
      respArr.data = {};
      callback(true, respArr);
      return false;
    }
    if (field === 'phone' && fieldValue.length !== 10) {
      isValidated = false;
      respArr.code = constants.invalid_phone;
      respArr.msg = global.lang.invalid_phone;
      respArr.data = {};
      callback(true, respArr);
      return false;
    }
  }
  if (isValidated) {
    callback(false);
  }
  return isValidated;
};

exports.getResponse = (code, msg, data) => {
  respArr = {};
  respArr.code = code;
  respArr.msg = msg;
  respArr.data = data;
  return respArr;
};

exports.sendMail = async (mailData) => {
  ejs.renderFile(`${global.filePath}/app/views/mailer/verify_email.ejs`, mailData, (err, mailTemplate) => {
    const msg = {
      to: mailData.email,
      from: 'support@myryde.app',
      subject: mailData.subject,
      text: 'HI',
      html: mailTemplate,
    };
    sgMail.send(msg, (error) => {
      if (error) {
        throw error;
      } else {
        console.log('Email sent: ');
      }
    });
  });
};

exports.getOtp = () => Math.floor(1000 + Math.random() * 9000);

exports.checkParams = (req, reqParams) => {
  let isValidated = true;
  return new Promise((resolve) => {
    console.log(reqParams);
    for (let i = 0; i < reqParams.length; i++) {
      const field = reqParams[i];
      const fieldValue = (req[field]) ? req[field] : '';
      if (fieldValue.length === 0) {
        isValidated = false;
        respArr.code = constants.param_missing;
        respArr.msg = global.lang.param_missing;
        respArr.data = {};
        resolve(respArr);
        return false;
      }
      if (field === 'phone' && (fieldValue.length < 8 || fieldValue.length > 10)) {
        isValidated = false;
        respArr.code = constants.invalid_phone;
        respArr.msg = global.lang.invalid_phone;
        respArr.data = {};
        resolve(respArr);
        return false;
      }
    }
    if (isValidated) {
      respArr.code = constants.success;
      respArr.msg = global.lang.success;
      respArr.data = {};
      resolve(respArr);
    }
  });
};
