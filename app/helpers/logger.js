const appRoot = require('app-root-path');
const winston = require('winston');

const tsFormat = () => (new Date()).toLocaleTimeString();

const logger = winston.createLogger({
  transports: [
    new (require('winston-daily-rotate-file'))({
      filename: `${appRoot}/logs/application-%DATE%.log`,
      timestamp: tsFormat,
      datePattern: 'YYYY-MM-DD',
      prepend: true,
      level: process.env.NODE_ENV === 'development' ? 'verbose' : 'info',
    }),
  ],
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
    colorize: true,
    level: 'debug',
    timestamp: tsFormat,
  }));
}

logger.stream = {
  write(message) {
    logger.info(message);
  },
};

module.exports = logger;
