const mongoose = require('mongoose');

// define the schema for our payment_model model
const payment = mongoose.Schema({
  account_number: String,
  bank_name: String,
  name_on_account: String,
  country: String,
  branch_id: String, // Swift Code/Ifsc Code
  user_id: {
    type: mongoose.Schema.Types.ObjectId, ref: 'User',
  },
  created_date: Date,
  updated_date: Date,
},
{
  versionKey: false,
});

exports = mongoose.model('payment', payment);
