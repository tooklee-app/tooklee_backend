// load the things we need
const mongoose = require('mongoose');

const drivingLicence = mongoose.Schema({
  licence_number: String,
  country: String,
  valid_till: Date, // Validity of driving licence
  licence_front_view: String, // Front Image of Driving Licence
  licence_back_view: String, // Back Image of Driving Licence
  user_id: {
    type: mongoose.Schema.Types.ObjectId, ref: 'User',
  },
  created_date: Date,
  updated_date: Date,
},
{
  versionKey: false,
});

exports = mongoose.model('driving_licence', drivingLicence);
