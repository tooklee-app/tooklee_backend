// load the things we need
const mongoose = require('mongoose');
// define the schema for our user model
const users = mongoose.Schema({
  first_name: String,
  last_name: String,
  email: String,
  country_code: String,
  otp: Number,
  account_status: {
    type: Number, default: 0,
  }, // 1: Setup completed and 0 Pending and 2 for blocked
  profile_pic: String,
  facebook_id: String,
  google_id: String,
  phone: String,
  dob: Date,
  is_email_verified: {
    type: Number, default: 0,
  },
  offer_ride_step: {
    type: Number, default: 0,
  }, // 0: No Step, 1: Vehicle detail added,Payment detail added,3 : 2 Driving licence Updated
  device_id: String,
  device_token: String,
  device_type: {
    type: Number, default: 0,
  }, // 1: Android and 2 iOS
  payment_pref: {
    type: Number, default: 0,
  }, // 1: Cash and 2: Wallet,3 Bank
  created_date: Date,
  updated_date: Date,
},
{
  versionKey: false,
});

// create the model for users and expose it to our app
module.exports = mongoose.model('users', users);
