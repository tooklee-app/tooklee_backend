const mongoose = require('mongoose');

// define the schema for our Vehicle model
const vehicle = mongoose.Schema({
  vehicle_brand: String,
  vehicle_name: String,
  vehicle_type: { type: Number }, // 1: Sedan, 2: Hatchback
  mf_year: Number, // year of manufacturing
  is_pet_allowed: { type: Number }, // 1: Allowed, 0: Not Allowed
  is_smoking_allowed: { type: Number }, // 1: Allowed, 0: Not Allowed
  vehicle_image: [String],
  user_id: {
    type: mongoose.Schema.Types.ObjectId, ref: 'User',
  },
  created_date: Date,
  updated_date: Date,
},
{
  versionKey: false,
});

exports = mongoose.model('vehicle', vehicle);
