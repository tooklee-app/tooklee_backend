// load the things we need
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/**
 * Schema for Vehicle manufacturer
 */
const manufacturer = Schema({
  manufacturer_name: String,
  created_date: Date,
  updated_date: Date,
  vehicle_detail: [String],
},
{
  versionKey: false,
});

// create the model for vehicle and expose it to our app
module.exports = {
  manufacturer: mongoose.model('manufacturer', manufacturer),
};
