const mongoose = require('mongoose');
const vehicle_detail = require('../../models/manage_vehicles');


/**
 * @description Load the signup page
 * @param {*} req
 * @param {*} res
 */
exports.add_vehicles = async (req, res) => {
  try {
    const vehicle_detail_model = vehicle_detail.manufacturer;
    const manufacturer = await vehicle_detail_model.find({}).exec();
    res.render('manage_vehicle/index.ejs', { manufacturer });
  } catch (e) {
    console.log('Error', e.message);
  }
};

exports.save_Data = async (req, res) => {
  try {
    let reqType = req.body.reqType;
    reqType = (typeof reqType !== 'undefined') ? reqType : 1;
    const data = req.body.data;
    if (reqType === 1) {
      const manufacturer = { manufacturer_name: data.manufacturer };
      const vehicle_detail_model = vehicle_detail.manufacturer;
      const vehicle_info = new vehicle_detail_model(manufacturer);
      vehicle_info.save();
      res.status(200).json({ msg: 'Manufacturer Info Saved' });
    } else {
      const manufacturer = vehicle_detail.manufacturer;
      const mfId = mongoose.Types.ObjectId(data.manufacturer_id);
      const condition = { _id: mfId };
      const update = { $addToSet: { vehicle_detail: data.model_name } };
      const updateDetail = await manufacturer.findOneAndUpdate(condition, update).exec();
      console.log('updateDetail.......', updateDetail);
      res.status(200).json({ msg: 'Vehicle Model Detail Saved Successfully' });
    }
  } catch (e) {
    res.send(200).json(e.error);
  }
};
