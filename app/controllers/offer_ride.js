const dateFormat = require('dateformat');
const userModel = require('../models/user');
const paymentModel = require('../models/payment');
const drivingLicense = require('../models/drivingLicense');
const vehicle = require('../models/vehicle');
const vehicle_detail = require('../models/manage_vehicles');
const http_code = require('../constants/http_codes');
const commonfn = require('../helpers/commonfn');
const lang = require('../constants/en_messages');

/**
 * @description Get Addtional information of user like driving licence,banking info and
 * vehicles detail
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

exports.getDrivingLicense = async (req, res) => {
  try {
    const user_info = req.user;
    const condition = { user_id: user_info._id };
    const select = { _id: 0, user_id: 0 };
    const req_info = await drivingLicense.findOne(condition, select);
    if (req_info != null) {
      res.json(commonfn.getResponse(http_code.success, lang.success, req_info));
    } else {
      res.json(commonfn.getResponse(http_code.error, lang.no_data, {}));
    }
  } catch (err) {
    res.json(commonfn.getResponse(http_code.error, err.message, {}));
  }
};

exports.getVehicle = async (req, res) => {
  try {
    const user_info = req.user;

    const condition = { user_id: user_info._id };
    const select = { _id: 0, user_id: 0 };
    const req_info = await vehicle.findOne(condition, select);
    if (req_info != null) {
      res.json(commonfn.getResponse(http_code.success, lang.success, req_info));
    } else {
      res.json(commonfn.getResponse(http_code.error, lang.no_data, {}));
    }
  } catch (err) {
    res.json(commonfn.getResponse(http_code.error, err.message, {}));
  }
};
/**
*   Add Vehicle Detail of User
*/
exports.manageVehicle = async (req, res) => {
  try {
    const {
      vehicle_name, vehicle_brand, vehicle_type, mf_year,
      is_pet_allowed, is_smoking_allowed, vehicle_image,
    } = req.body;


    const mandatoryParms = ['vehicle_name', 'vehicle_type', 'mf_year'];
    const validationResp = await commonfn.checkParams(req.body, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }

    const vehicle_info = {
      vehicle_name,
      vehicle_type,
      vehicle_brand,
      mf_year,
      is_pet_allowed,
      is_smoking_allowed,
      vehicle_image: JSON.parse(vehicle_image),
      updated_date: dateFormat(),
    };

    const user_id = req.user._id;
    let condition = { user_id };
    const select = { fields: { _id: 1 }, new: true };
    /**
         * Check if there is some vehicle information is saved in respect to this user
         */
    const user_data = await vehicle.findOneAndUpdate(condition, vehicle_info, select).exec();

    if (user_data == null) {
      vehicle_info.user_id = user_id;
      vehicle_info.created_date = dateFormat();
      const vehicle_detail = new vehicle(vehicle_info);
      vehicle_detail.save();
      condition = { _id: user_id };
      const update = {
        offer_ride_step: (user_info.offer_ride_step !== 0) ? user_info.offer_ride_step : 1,
      };
      await userModel.update(condition, update);
    }
    delete vehicle_info.user_id;
    res.json(commonfn.getResponse(http_code.success, lang.success, vehicle_info));
  } catch (err) {
    res.json(commonfn.getResponse(http_code.error, err.message, {}));
  }
};

exports.managePayment = async (req, res) => {
  try {
    const {
      account_number, bank_name, branch_id, name_on_account, country,
    } = req.body;
    /*
         * Verify the token
         */
    const mandatoryParms = ['account_number', 'bank_name', 'branch_id', 'name_on_account', 'country'];
    const validationResp = await commonfn.checkParams(req.body, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }

    const payment_info = {
      account_number,
      bank_name,
      branch_id,
      name_on_account,
      country,
      updated_date: dateFormat(),
    };

    const user_id = req.user._id;
    let condition = { user_id };
    const select = { fields: { _id: 1 }, new: true };
    const user_data = await paymentModel.findOneAndUpdate(condition, payment_info, select).exec();
    if (user_data == null) {
      payment_info.user_id = user_id;
      payment_info.created_date = dateFormat();
      const payment_detail = new paymentModel(payment_info);
      payment_detail.save();
      condition = { _id: user_id };
      const update = {
        offer_ride_step: (user_info.offer_ride_step !== 1) ? user_info.offer_ride_step : 2,
      };
      await userModel.update(condition, update);
    }
    delete payment_info.user_id;
    res.json(commonfn.getResponse(http_code.success, lang.success, payment_info));
  } catch (err) {
    res.json(commonfn.getResponse(http_code.error, err.message, {}));
  }
};

exports.manageDrivingLicence = async (req, res) => {
  try {
    const {
      licence_number, valid_till, licence_front_view, licence_back_view, country,
    } = req.body;

    const mandatoryParms = ['licence_number', 'valid_till', 'licence_front_view', 'licence_back_view', 'country'];
    const validationResp = await commonfn.checkParams(req.body, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }

    const payment_info = {
      licence_number,
      valid_till,
      licence_front_view,
      licence_back_view,
      country,
      updated_date: dateFormat(),
    };

    const user_id = req.user._id;
    let condition = { user_id };
    const select = { fields: { _id: 1 }, new: true };
    const user_data = await drivingLicense.findOneAndUpdate(condition, payment_info, select).exec();
    if (user_data == null) {
      payment_info.user_id = user_id;
      payment_info.created_date = dateFormat();
      const payment_detail = new driving_licence_model(payment_info);
      payment_detail.save();
      condition = { _id: user_id };
      const update = {
        offer_ride_step: (user_info.offer_ride_step !== 2) ? user_info.offer_ride_step : 3,
      };
      await userModel.update(condition, update);
    }
    delete payment_info.user_id;
    res.json(commonfn.getResponse(http_code.success, lang.success, payment_info));
  } catch (err) {
    res.json(commonfn.getResponse(http_code.error, err.message, {}));
  }
};

exports.getVehicles = async (req, res) => {
  try {
    const { manufacturer } = vehicle_detail.manufacturer;
    const projection = { vehicle_detail: 1, manufacturer_name: 1, _id: 0 };
    const vehicles = await manufacturer.find({}, projection);
    res.json(commonfn.getResponse(http_code.success, lang.success, vehicles));
  } catch (err) {
    res.json(commonfn.getResponse(http_code.error, err.message, {}));
  }
};
