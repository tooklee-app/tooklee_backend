const dateFormat = require('dateformat');
const config = require('config');
const jwt = require('jsonwebtoken');
const user_model = require('../models/user');
const commonfn = require('../helpers/commonfn');
const http_code = require('../constants/http_codes');
const logger = require('../helpers/logger');

const respArr = {};

/**
 *
 * @description Used for login to app
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.index = async function (req, res) {
  const body = req.body;
  const phone = body.phone;
  const country_code = body.country_code;
  const otp = commonfn.getOtp();
  try {
    /*
        * Validate the mandatory Params
        */
    const mandatoryParms = ['phone', 'country_code'];

    const validationResp = await commonfn.checkParams(body, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }
    /*
         * Check if user is already registered then update otp otherwise return error
         */
    const condition = { phone, country_code };
    const update = { otp };
    const user = await user_model.findOneAndUpdate(condition, update).exec();

    if (user != null) {
      /*
             *  IF User is already Registered
             */
      const mobileNo = country_code + phone;
      commonfn.sendOtp(mobileNo, otp);
      res.json(commonfn.getResponse(http_code.success, lang.otp_sent_success, { token: generateToken(user._id) }));
    } else {
      res.json(commonfn.getResponse(http_code.error, lang.invalid_login, {}));
    }
  } catch (err) {
    logError(err, req);
    respArr.code = http_code.error;
    respArr.msg = (err.message) ? err.message : lang.error;
    respArr.data = {};
    res.json(respArr);
  }
};
/**
 *
 * @description Register a user
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.register = async (req, res) => {
  const postReq = req.body;
  const phone = postReq.phone;
  const country_code = postReq.country_code;
  const otp = commonfn.getOtp();
  try {
    /*
        * Validate the mandatory Params
        */
    const mandatoryParms = ['phone', 'country_code'];

    const validationResp = await commonfn.checkParams(postReq, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }
    /*
         * Check if user is already registered then update otp otherwise create a new user
         */
    const condition = { phone, country_code };
    const update = { otp };
    const user = await user_model.findOneAndUpdate(condition, update).exec();

    if (user != null) {
      /*
             *  IF User is already Registered
             */
      res.json(commonfn.getResponse(http_code.error, lang.phone_exists, {}));
    } else {
      /*
             * Register user if not registered with given mobile no.
             */
      const user_detail = new user_model({
        phone,
        country_code,
        otp,
        created_date: dateFormat(),
        updated_date: dateFormat(),
      });
      const insert_detail = await user_detail.save();
      const mobileNo = country_code + phone;
      commonfn.sendOtpCode(mobileNo, otp);
      res.json(commonfn.getResponse(http_code.success, lang.otp_sent_success, { token: generateToken(insert_detail._id) }));
    }
  } catch (err) {
    logError(err, req);
    respArr.code = http_code.error;
    respArr.msg = (err.message) ? err.message : lang.error;
    respArr.data = {};
    res.json(respArr);
  }
};
/*
 * Verify Otp Method
 */
exports.verifyOtp = async function (req, res, next) {
  const postReq = req.body;
  const user = req.user;
  const otp = postReq.otp;
  try {
    /*
         * Validate Parameters
         */
    const mandatoryParms = ['otp'];
    const validationResp = await commonfn.checkParams(postReq, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }
    /*
        *   is_email_verification: 1 means verification if for email and 2  means phone verification
        */
    const verification_type = (postReq.is_email_verification) ? postReq.is_email_verification : 2;

    const condition = { _id: user.id };
    const update = (verification_type === 1) ? { otp: 0, is_email_verified: 1 } : { otp: 0 };
    const field = { _id: 0 };
    const user_detail = await user_model.findOne(condition, field).exec();
    if (user_detail == null) {
      respArr.code = http_code.token_expired;
      respArr.msg = global.lang.token_expired;
      respArr.data = {};
      res.json(respArr);
    } else if (user_detail.otp.toString() === otp || otp === config.get('bypass_otp')) {
      if (typeof user_detail.profile_pic !== 'undefined' && user_detail.profile_pic !== '' && user_detail.profile_pic.length < 30) {
        console.log(`---Profile pic---${user_detail.profile_pic}`);
        user_detail.profile_pic = host + config.get('upload_path') + user_detail.profile_pic;
      }
      if (verification_type === 1) {
        user_detail.is_email_verified = 1;
      }
      console.log(update);
      user_model.update(condition, update).exec();
      res.json(commonfn.getResponse(http_code.success, lang.otp_verified, user_detail));
    } else {
      res.json(commonfn.getResponse(http_code.invalid_otp, lang.invalid_otp, {}));
    }
  } catch (err) {
    logError(err, req);
    respArr.code = http_code.error;
    respArr.msg = (err.message) ? err.message : lang.error;
    respArr.data = {};
    res.json(respArr);
  }
  // });
};
/*
* Change Otp on Email.
*/
exports.send_otp_email = async function (req, res, next) {
  try {
    const postReq = req.body;
    const mandatoryParms = ['email'];
    const validationResp = await commonfn.checkParams(postReq, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }
    const otp = commonfn.getOtp();
    const email = postReq.email.toLowerCase();
    let reqType = postReq.reqType;
    reqType = (typeof reqType !== 'undefined') ? reqType : 1;
    const condition = { email };
    const fields = { fields: { _id: 1, first_name: 1 } };
    const update = { otp };
    const user_info = await user_model.findOneAndUpdate(condition, update, fields).exec();
    /**
         * This condition means that we have to check for email to exist and then send otp
         */
    if (reqType === 1) {
      if (user_info != null) {
        const subject = 'Verify you Email';
        const message = `Your otp is ${otp}. Please do not share it with anybody`;
        const mailData = {};
        mailData.email = email;
        mailData.subject = subject;
        mailData.message = message;
        mailData.first_name = user_info.first_name;
        commonfn.sendMail(mailData);
        res.json(commonfn.getResponse(http_code.success, lang.email_otp_sent_success, { token: generateToken(user_info._id) }));
      } else {
        res.json(commonfn.getResponse(http_code.error, lang.email_not_exists, {}));
      }
    } else if (user_info == null) {
      const subject = 'Verify you Email';
      const message = `Your otp is ${otp}. Please do not share it with anybody`;
      const mailData = {};
      mailData.email = email;
      mailData.subject = subject;
      mailData.message = message;
      mailData.first_name = '';
      commonfn.sendMail(mailData);
      res.json(commonfn.getResponse(http_code.success, lang.email_otp_sent_success, { otp }));
    } else {
      res.json(commonfn.getResponse(http_code.error, lang.email_exists, {}));
    }
  } catch (err) {
    logError(err, req);
    respArr.code = http_code.error;
    respArr.msg = (err.message) ? err.message : lang.error;
    respArr.data = {};
    res.json(respArr);
  }
};

/*
* Change Otp on Phone.
*/
exports.send_otp_phone = async function (req, res, next) {
  try {
    const postReq = req.body;
    const mandatoryParms = ['phone', 'country_code'];
    const validationResp = await commonfn.checkParams(postReq, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }
    const otp = commonfn.getOtp();
    let { phone, country_code, reqType } = postReq;
    reqType = (typeof reqType !== 'undefined') ? reqType : 1;
    const condition = { phone };
    const fields = { fields: { _id: 1 } };
    const update = { otp };
    const data = await user_model.findOneAndUpdate(condition, update, fields).exec();
    /**
         * This condition means that we have to check for mobile no exist and then send otp
         */
    if (reqType === 1) {
      if (data != null) {
        const mobileNo = country_code + phone;
        commonfn.sendOtp(mobileNo, otp);
        res.json(commonfn.getResponse(http_code.success, lang.otp_sent_success, { token: generateToken(data._id) }));
      } else {
        res.json(commonfn.getResponse(http_code.error, lang.phone_not_exists, {}));
      }
    } else if (data === null) {
      const mobileNo = country_code + phone;
      commonfn.sendOtp(mobileNo, otp);
      res.json(commonfn.getResponse(http_code.success, lang.otp_sent_success, { otp }));
    } else {
      res.json(commonfn.getResponse(http_code.error, lang.phone_exists, {}));
    }
  } catch (err) {
    logError(err, req);
    respArr.code = http_code.error;
    respArr.msg = (err.message) ? err.message : lang.error;
    respArr.data = {};
    res.json(respArr);
  }
};

/**
 * @description create a session of a user after loggin In
 * @param {*} req
 * @param {*} res
 */
exports.createSession = async function (req, res) {
  try {
    const postReq = req.body;
    const { device_id, device_token, device_type } = postReq;
    const mandatoryParms = ['device_id', 'device_type'];
    const validationResp = await commonfn.checkParams(postReq, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }

    const condition = { _id: req.user.id };
    const fields = { _id: 0, new: true };
    const update = { device_id, device_token, device_type };
    const data = await user_model.findOneAndUpdate(condition, update, fields).exec();
    if (data != null) {
      res.json(commonfn.getResponse(http_code.success, lang.success, data));
    } else {
      res.json(commonfn.getResponse(http_code.error, lang.token_expired, {}));
    }
  } catch (err) {
    logError(err, req);
    respArr.code = http_code.error;
    respArr.msg = (err.message) ? err.message : lang.error;
    respArr.data = {};
    res.json(respArr);
  }
};
/**
 * @description Update phone no for a user
 * @param {*} req
 * @param {*} res
 */
exports.updatePhone = async function (req, res) {
  try {
    const postReq = req.body;
    const user = req.user;
    const mandatoryParms = ['phone', 'country_code'];
    const validationResp = await commonfn.checkParams(postReq, mandatoryParms);
    if (validationResp.code !== 200) {
      res.json(validationResp);
    }
    const otp = commonfn.getOtp();
    const phone = postReq.phone;
    const country_code = postReq.country_code;
    const condition = { phone };
    const fields = { _id: 1 };

    const data = await user_model.findOne(condition, fields).exec();
    if (data == null) {
      const mobileNo = country_code + phone;
      commonfn.sendOtp(mobileNo, otp);
      const condition = { _id: user.id };
      const update = { phone, otp };
      const user = await user_model.update(condition, update).exec();
      res.json(commonfn.getResponse(http_code.success, lang.otp_sent_success, { token: generateToken(user._id) }));
    } else if (data._id === user.id) {
      res.json(commonfn.getResponse(http_code.error, lang.same_phone, {}));
    } else {
      res.json(commonfn.getResponse(http_code.error, lang.phone_exists, {}));
    }
  } catch (err) {
    logError(err, req);
    respArr.code = http_code.error;
    respArr.msg = (err.message) ? err.message : lang.error;
    respArr.data = {};
    res.json(respArr);
  }
};
/**
 *
 * @description Delete the account of a user
 * @param {*} req
 * @param {*} res
 */
exports.deleteAccount = async function (req, res) {
  const phone = req.body.phone;
  const condition = { phone };
  const response = await user_model.remove(condition).exec();
  console.log(condition);
  res.send('This Account is Deleted Successfully');
};

function generateToken(modelId) {
  return jwt.sign({ id: modelId }, process.env.SECRET, {
    expiresIn: (3600 * 7 * 24), // expires in 7 days
  });
}

function logError(error, req) {
  logger.error(error);
  logger.error(req.body);
}
