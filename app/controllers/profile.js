const dateFormat = require('dateformat');
const user_model = require('../models/user');
const constants = require('../constants/http_codes');
const commonfn = require('../helpers/commonfn');
const lang = require('../constants/en_messages');
const multiparty = require('multiparty');
const config = require('config');

const respArr = {};

exports.index = async function (req, res, next) {
    try {
        const formdata = await getFormData(req);
        let { email, first_name, last_name, dob, google_id, facebook_id, profile_pic, is_email_verified } = formdata.body;
        const file = formdata.files;
        let query, fields;
        /*
         * Verify the token
         */
        const userId = req.user.id;
        if (file.profile_pic && file.profile_pic.length > 0) {
            const validTypes = ['jpg', 'png', 'jpeg'];
            let imgValidationResp = await commonfn.validateImage(file.profile_pic[0], validTypes);
            if (imgValidationResp.code !== constants.success) {
                console.log('------Upload Image Err-----');
                res.json(imgValidationResp);
            } else {
                profile_pic = imgValidationResp.data;
            }
        } else {
            profile_pic = (profile_pic) ? profile_pic[0] : '';
        }

        const mandatoryParms = ['email', 'first_name', 'last_name', 'dob'];
        const validationResp = await commonfn.checkParams(formdata.body, mandatoryParms);
        if (validationResp.code !== 200) {
            res.json(validationResp);
        }
        email = (email) ? (email[0].toLowerCase()) : '';
        first_name = (first_name) ? first_name[0] : '';
        last_name = (last_name) ? last_name[0] : '';
        facebook_id = (facebook_id) ? facebook_id[0] : '';
        google_id = (google_id) ? google_id[0] : '';
        is_email_verified = (is_email_verified) ? is_email_verified[0] : 1;
        dob = (dob) ? dob[0] : '';

        query = { 'email': email };
        fields = { '_id': 1 };

        console.log('-------Checking if Email is already Registered---------');
        let user_info = await user_model.findOne(query, fields).exec();
        if (user_info == null || user_info._id === userId) {
            let userData = {
                email: email,
                first_name: first_name,
                last_name: last_name,
                account_status: 1,
                facebook_id: facebook_id,
                google_id: google_id,
                is_email_verified: is_email_verified,
                dob: dob,
                profile_pic: profile_pic,
                updated_date: dateFormat('isoDateTime'),
            };
            query = { '_id': userId };
            fields = { '_id': 0, new: true };
            user_info = await user_model.findOneAndUpdate(query, userData, fields);
            if (user_info.profile_pic !== "" && user_info.profile_pic.length < 30) {
                user_info['profile_pic'] = host + config.get('upload_path') + user_info['profile_pic'];
            }
            res.json(commonfn.getResponse(constants.success, lang.success, user_info));
        } else {
            res.json(commonfn.getResponse(constants.error, lang.email_exists, {}));
        }
    } catch (err) {
        respArr['code'] = constants.error;
        respArr['msg'] = (err.message) ? err.message : lang.error;
        respArr['data'] = {};
        res.json(respArr);
    }
};

exports.updateProfile = async (req, res) => {
    try {
        const body = req.body;
        let { email, phone, first_name, last_name, dob, profile_pic, payment_pref } = body;
        const update = {};
        let data, condition, select;
        condition = { '_id': req.user.id };
        select = { '_id': 1 };
        data = await user_model.findOne(condition, select).exec();
        if (data == null) {
            res.json(commonfn.getResponse(constants.token_expired, lang.token_expired, {}));
        } else {
            /*
            * validate if email if already registered
            */
            if (typeof email !== 'undefined' && email.length !== 0) {
                condition = { 'email': email };
                data = await user_model.findOne(condition, select).exec();
                if (data != null) {
                    res.json(commonfn.getResponse(constants.error, lang.email_exists, {}));
                } else {
                    update['email'] = email;
                }
            }
            if (typeof phone !== 'undefined' && phone.length !== 0) {
                condition = { 'phone': phone };
                data = await user_model.findOne(condition).exec();
                if (data != null) {
                    res.json(commonfn.getResponse(constants.error, lang.phone_exists, {}));
                } else {
                    update['phone'] = phone;
                }
            }

            if (typeof profile_pic !== 'undefined' && profile_pic.length > 0) {
                update['profile_pic'] = profile_pic;
            }
            if (typeof dob !== 'undefined' && dob.length !== 0) {
                update['dob'] = dob;
            }
            if (typeof first_name !== 'undefined' && first_name.length !== 0) {
                update['first_name'] = first_name;
            }
            if (typeof last_name !== 'undefined' && last_name.length !== 0) {
                update['last_name'] = last_name;
            }
            if (typeof payment_pref !== 'undefined' && payment_pref.length !== 0) {
                update['payment_pref'] = payment_pref;
                update['offer_ride_step'] = 2;
            }
            condition = { '_id': req.user.id };
            select = { fields: { '_id': 0, 'otp': 0 }, new: true };
            data = await user_model.findOneAndUpdate(condition, update, select);
            res.json(commonfn.getResponse(constants.success, lang.success, data));
        }

    } catch (err) {
        respArr['code'] = constants.error;
        respArr['msg'] = err.message;
        respArr['data'] = {};
        res.json(respArr);
    }
};

getFormData = function (req) {
    return new Promise((resolve, reject) => {
        const form = new multiparty.Form();
        form.parse(req, function (err, body, file) {
            if (err) {
                reject(err);
            } else {
                const returnArr = {};
                returnArr['body'] = body;
                returnArr['files'] = file;
                resolve(returnArr);
            }
        });
    });
};

