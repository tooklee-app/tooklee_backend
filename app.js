require('dotenv').config();
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const helmet = require('helmet');
const compression = require('compression');

const logger = require('./app/helpers/logger');

const app = express();
const port = process.env.PORT || 3000;

global.lang = require('./app/constants/en_messages');

global.filePath = __dirname;

function setHost(req, res, next) {
  const reqProtocol = req.protocol;
  console.log(reqProtocol);
  console.log(req.headers.host);
  global.host = `${reqProtocol}://${req.headers.host}`;
  console.log(global.host);
  next();
}

require('./docs')(app);

app.use(setHost);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser()); // read cookies (needed for auth)
app.use(helmet());
app.use(compression());
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'ejs');
app.use(morgan('combined', { stream: logger.stream }));

mongoose.connect(process.env.DB_URL, { useNewUrlParser: true }, (error) => {
  if (error) throw error;
  logger.info('Connected to DB');
});

// routes ======================================================================
require('./app/routes.js')(app); // load our routes and pass in our app and fully configured passport

app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // add this line to include winston logging
  logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  // render the error page
  const status = err.status || 500;
  res.status(status).render(status, { title: 'Sorry, page not found' });
});

// launch ======================================================================
app.listen(port, (error) => {
  if (error) throw error;
  logger.info(`Server Started: ${port}`);
});

module.exports = app;
